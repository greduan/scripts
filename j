#!/bin/sh

JOURNAL_DIR=${JOURNAL_DIR:="$HOME/.journals"}
test -e "$JOURNAL_DIR" || mkdir "$JOURNAL_DIR"

JOURNAL_FILE="$(ls -1t $JOURNAL_DIR | dmenu)"
test -n "$JOURNAL_FILE" || exit 1

ENTRY_TEXT="$(dmenu_arbitrary)"
test -n "$ENTRY_TEXT" || exit 1

ENTRY="$ENTRY_TEXT:$(date)"

echo "$ENTRY" >> "$JOURNAL_DIR/$JOURNAL_FILE"
